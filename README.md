## logos

### Usage
To run `logos` with a file name input, you need to use the `--file` tag.
After building, try:

Try: `./logos --file websites.csv`

There is a `websites.nix` file in the base directory for testing. `logos` automatically writes the csv into the same
directory with it's file source, but with a string appended to it to show its the result. It writes two files. One is the `*_results.csv`. The other is an error file. Its a `.csv` too, and can pass, i suppose for a structured log. My thinking is that we could use thse as source inputs for retries, only by extending `logos` itself a little.

`logos` aslo takes a comma-separated list of urls. To use it this way, no flags are needed.