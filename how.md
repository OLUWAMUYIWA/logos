Nodes and elements explanation: [here](https://stackoverflow.com/questions/9979172/difference-between-node-object-and-element-object)

OG and Twitter logos [here](https://css-tricks.com/essential-meta-tags-social-media/)

More Meta [here](https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/itemprop)

Reducing allocations [here](https://methane.github.io/2015/02/reduce-allocation-in-go-code/)


###
Todo Production: 
Add Contexts for cancellation and timeouts