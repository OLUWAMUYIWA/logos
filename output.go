package main

import (
	"bufio"
	"encoding/csv"
	"log"
	"os"
)

// Csv wraps a `*csv.Writer`. Its used to write the output to csv files
type Csv struct {
	wtr *csv.Writer
}

// Result represents the result of scraping one site
type Result struct {
	domain, logoUrl, favUrl string
}

// NewCsvAriter creates a new Csv writer
func NewCsvAriter(path string) (*Csv, error) {
	wtr, err := os.Create(path)
	bWtr := bufio.NewWriter(wtr)
	if err != nil {
		return nil, err
	}
	csvWtr := csv.NewWriter(bWtr)

	return &Csv{wtr: csvWtr}, nil
}

// WriteResults writes the results into a csv file
func (c *Csv) WriteResults(resChan <-chan Result) error {
	for res := range resChan {
		if err := c.wtr.Write([]string{res.domain, res.logoUrl, res.favUrl}); err != nil {
			return err
		}
	}
	c.wtr.Flush()
	// return any error from flush
	return c.wtr.Error()
}

// WriteErrs writes errors in csv format
func (c *Csv) WriteErrs(errChan <-chan ErrResp) error {
	for err := range errChan {
		if err := c.wtr.Write([]string{err.addr, err.err}); err != nil {
			return err
		}
	}
	c.wtr.Flush()
	// return any error from flush
	return c.wtr.Error()
}

// LogErrorswrites errors in csv format
func LogErrors(errors <-chan ErrResp, errLog string) {
	// errFunc writes to a log
	// TODO: we may choose to retry these ones later
	errFunc := func(errs <-chan ErrResp) {
		cvs, err := NewCsvAriter(errLog)
		if err != nil {
			// TODO do something
			log.Println("Could not create csv writer so no error logs")
			return
		}
		err = cvs.WriteErrs(errs)
		if err != nil {
			// TODO do something
			return
		}
	}
	go func() {
		errFunc(errors)
	}()
}
