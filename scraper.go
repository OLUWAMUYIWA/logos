package main

import (
	"bytes"
	"crypto/tls"
	"fmt"
	"io"
	"net/http"
	"strings"
	"sync"
	"time"
)

const myUserAgent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.5112.79 Safari/537.36"

// Scraper is the engine of this application
// stores a client, concurrently sends requests and drives the scraping of websites
type Scraper struct {
	addrs []string
	cl    *http.Client
}

type KV struct {
	k, v []byte
}

// NewScraper creates a new Scraper
func NewScraper(addrs []string) *Scraper {
	// preprocess addrs
	pref := "https://"
	for i := 0; i < len(addrs); i++ {
		addr := addrs[i]
		if !strings.HasPrefix(addr, pref) {
			addrs[i] = strings.Join([]string{pref, addr}, "")
		}
	}
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{
			InsecureSkipVerify: true,
		},
	}
	cl := &http.Client{
		Transport: tr,
		Timeout:   50 * time.Second, // maybe too much, but most o the errors i've gotten are network timeouts
	}
	return &Scraper{
		addrs, cl,
	}
}

type ErrResp struct {
	err  string
	addr string
}

// StreamGet takes a bound which specifies how many requests we wish to launch at once
// It's a way to keep the number of goroutines running in check
// Returns two channels, one for a stream of KVs, another ofr a stream of errors which may be logged or retried
// TODO: change to NewRequestWithContext
func (s *Scraper) StreamGet(bound int) (<-chan *KV, <-chan ErrResp) {
	// sephamore chan to set concurrency limit
	sephChan := make(chan struct{}, bound)
	respsChan := make(chan *KV)
	errChan := make(chan ErrResp)
	var wg sync.WaitGroup
	wg.Add(len(s.addrs))
	done := func() {
		wg.Done()
	}

	f := func(addr *string, cl *http.Client) {
		// add one more to the sephamore channel
		// this blocks the moment the bounds limit is reached
		sephChan <- struct{}{}
		req, err := http.NewRequest("GET", *addr, nil)
		req.Header.Set("User-Agent", myUserAgent)
		if err != nil {
			errChan <- ErrResp{err.Error(), *addr}
		}
		resp, err := cl.Do(req)
		if err != nil {
			errChan <- ErrResp{err.Error(), *addr}
		} else {
			contentType := resp.Header.Get("Content-Type")
			// comeback is 200/OK the only valid status code we care about?
			if resp.StatusCode != http.StatusOK {
				errChan <- ErrResp{err: fmt.Sprintf("HTTP Error: Status code: %d", resp.StatusCode), addr: *addr}
			} else if !strings.HasPrefix(contentType, "text/html") {
				// check if its html
				errChan <- ErrResp{err: fmt.Sprintf("response content type was %s, not text/html", contentType), addr: *addr}
			} else {
				b, err := io.ReadAll(resp.Body)
				resp.Body.Close()
				if err != nil {
					errChan <- ErrResp{err: fmt.Sprintf("HTTP Error: Could not read body: %s", err), addr: *addr}
				} else {
					respsChan <- &KV{k: []byte(*addr), v: b}
				}
			}
		}

		// read one off the sephamore chan. makes sure that one is off the chan, and if there's goroutines waiting
		// for a free spot in the channel, lets them run
		<-sephChan
		done()
	}
	for _, addr := range s.addrs {
		addr := addr
		go f(&addr, s.cl)
	}

	go func() {
		wg.Wait()
		close(respsChan)
		close(errChan)
		close(sephChan)
	}()

	return respsChan, errChan
}

// Scrape does the wrork of parsing and fetching the logo and favicon urls concurrently
func (s *Scraper) Scrape(kvs <-chan *KV) <-chan Result {
	resChan := make(chan Result)

	drive := func() {
		for kv := range kvs {
			v := bytes.NewReader(kv.v)
			doc, err := NewDocument(v)
			if err != nil {
				// TODO
			}
			logo, fav := doc.Match()
			res := Result{domain: string(kv.k), logoUrl: string(logo), favUrl: string(fav)}
			resChan <- res
		}
	}

	go func() {
		drive()
		close(resChan)
	}()
	return resChan
}
