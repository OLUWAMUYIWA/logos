package main

type LogosError struct {
	msg string
}

func (l *LogosError) Error() string {
	return l.msg
}

type HttpError struct {
}
