package main

import (
	"fmt"
	"log"
	"os"
	"path"
	"strings"
	"sync"
	"testing"
)

func TestAll(t *testing.T) {

	var addrs []string
	var err error // comeback: necessary?
	pathOrUrl := "./testfiles/addrs1.txt"
	addrs, err = parseAddrsFromFile(pathOrUrl)
	if err != nil {
		fmt.Printf("Could not parse from file: %s", err)
		os.Exit(1)
	}

	scraper := NewScraper(addrs)
	var wg sync.WaitGroup
	wg.Add(2)

	kvs, errs := scraper.StreamGet(20)

	// handle all network errors by logging them
	// we might choose to retry based on err type
	errPath := ""
	dir, fname := path.Split(pathOrUrl)
	fname = strings.TrimSuffix(fname, ".csv")
	fname = fname + "_errors.csv"
	errPath = path.Join(dir, fname)

	go func() {
		LogErrors(errs, errPath)
		wg.Done()
	}()

	results := scraper.Scrape(kvs)

	outputPath := ""
	dir, fname = path.Split(pathOrUrl)
	fname = strings.TrimSuffix(fname, ".csv")
	fname = fname + "_logoresults.csv"
	outputPath = path.Join(dir, fname)

	wtr, err := NewCsvAriter(outputPath)

	if err != nil {
		log.Fatalf("Could not create csv writer: %s\n", err)
	}

	go func() {
		wtr.WriteResults(results)
		wg.Done()
	}()

	wg.Wait()
}
