package main

import (
	"io"
	"strings"

	"golang.org/x/net/html"
)

// Document represents an http document
// Its a wrapper around the *http.Node type
type Document struct {
	n *html.Node
}

// NewDocument creates a new document from an io.reder
// It expects to be given a reader parseable by html.Parse
func NewDocument(r io.Reader) (*Document, error) {
	root, err := html.Parse(r)
	return &Document{root}, err
}

// Match uses a series of matcher functions and the `Alt` combinator to scan nodes for
// logos and favicons
func (d *Document) Match() (string, string) {
	var logo, fav string

	var f func(*html.Node, int)
	f = func(n *html.Node, num int) {
		if n.Type == html.ElementNode {
			if n.Data == "meta" || n.Data == "img" {
				s := Alt(n, metaItemLogo, metaItemImg, ogLogo, twitterLogo, imgClass, imgAlt, imgSource)
				if s != "" {
					logo = s
					num++
				}
			}
		}
		if n.Data == "link" {
			s := Alt(n, faviconLinkClass, faviconLinkImage)
			if s != "" {
				fav = s
				num++
			}
		}
		if num == 2 {
			return
		}
		for c := n.FirstChild; c != nil; c = c.NextSibling {
			f(c, num)
		}
	}
	f(d.n, 0)
	return logo, fav
}

// Matcher is a func type used in Matcher combinators like `Alt“
type Matcher func(n *html.Node) string

// Alt is a sort of combinator. Ranges through a slice of matchers and returns as soon as
// it finds one which matches our tokens
func Alt(n *html.Node, matchers ...Matcher) string {
	for _, matcher := range matchers {
		res := matcher(n)
		if res != "" {
			return res
		}
	}
	return ""
}

// / MATCHER FUNCTIONS ///
// TODO: Virtually all of these functions can be conposed into one and parametarized
func imgClass(node *html.Node) string {
	addr := ""
	both := 0

	if strings.ToLower(node.Data) == "img" {
		for _, attr := range node.Attr {
			if attr.Key == "src" {
				addr = attr.Val
				both += 1
			}
			if attr.Key == "class" && strings.Contains(attr.Val, "logo") { // match
				both += 1
			}
			if both == 2 { // matches the "class=logo" attr and has an "src" attr
				return addr
			}
		}
	}

	// failed to find a match
	return ""
}

func imgAlt(node *html.Node) string {
	addr := ""
	both := 0
	if node.Data == "img" {
		for _, attr := range node.Attr {
			if attr.Key == "src" {
				addr = attr.Val
				both += 1
			}
			if attr.Key == "alt" && strings.Contains(attr.Val, "logo") { // match
				both += 1
			}
			if both == 2 { // matches the "alt=logo" attr and has an "src" attr
				return addr
			}
		}
	}

	// failed to find a match
	return ""
}

// ogLogo matches exts to HTML5 meta tags specified in the opengraph protocol: https://ogp.me/
func ogLogo(node *html.Node) string {
	addr := ""
	both := 0
	if node.Data == "meta" {
		for _, attr := range node.Attr {
			if attr.Key == "content" { // the img url
				addr = attr.Val
				both += 1
			}
			if attr.Key == "property" && attr.Val == "og:image" { // match
				both += 1
			}
			if both == 2 { // matches the "property=og:image" attr and has a "content" attr
				return addr
			}
		}
	}

	// failed to find a match
	return ""
}

// twitter_logo is twitter's proprietary version of the OG protocol
func twitterLogo(node *html.Node) string {
	addr := ""
	both := 0
	if node.Data == "meta" {
		for _, attr := range node.Attr {
			if attr.Key == "content" { // the img url
				addr = attr.Val
				both += 1
			}
			if attr.Key == "name" && attr.Val == "twitter:image" { // match
				both += 1
			}
			if both == 2 { // matches the "twitter:image" attr and has a "content" attr
				return addr
			}
		}
	}

	// failed to find a match
	return ""
}

func metaItemLogo(node *html.Node) string {
	addr := ""
	both := 0
	if node.Data == "meta" {
		for _, attr := range node.Attr {
			if attr.Key == "content" { // the img url
				addr = attr.Val
				both += 1
			}
			if attr.Key == "itemprop" && strings.Contains(attr.Val, "logo") { // match
				both += 1
			}
			if both == 2 { // matches the "itemprop:logo" attr and has a "content" attr
				return addr
			}
		}
	}

	// failed to find a match
	return ""
}

func metaItemImg(node *html.Node) string {
	addr := ""
	both := 0
	if node.Data == "meta" {
		for _, attr := range node.Attr {
			if attr.Key == "content" { // the img url
				addr = attr.Val
				both += 1
			}
			if attr.Key == "itemprop" && strings.Contains(attr.Val, "img") { // match
				both += 1
			}
			if both == 2 { // matches the "itemprop:img" attr and has a "content" attr
				return addr
			}
		}
	}

	// failed to find a match
	return ""
}

func imgSource(node *html.Node) string {
	if node.Data == "img" {
		for _, attr := range node.Attr {
			if attr.Key == "src" && strings.Contains(attr.Val, "logo") {
				return attr.Val
			}

		}
	}
	// failed to find a match
	return ""
}

func faviconLinkImage(node *html.Node) string {
	addr := ""
	both := 0
	if node.Data == "link" {
		for _, attr := range node.Attr {
			if attr.Key == "href" { // the img url
				addr = attr.Val
				both += 1
			}
			if attr.Key == "class" && strings.Contains(attr.Val, "favicon") { // match
				both += 1
			}
			if both == 2 { // matches the "itemprop:img" attr and has a "content" attr
				return addr
			}
		}
	}

	// failed to find a match
	return ""
}

func faviconLinkClass(node *html.Node) string {
	addr := ""
	both := 0
	if node.Data == "link" {
		for _, attr := range node.Attr {
			if attr.Key == "href" { // the img url
				addr = attr.Val
				both += 1
			}
			if attr.Key == "rel" && strings.Contains(attr.Val, "icon") { // match
				both += 1
			}
			if both == 2 { // matches the "itemprop:img" attr and has a "content" attr
				return addr
			}
		}
	}

	// failed to find a match
	return ""
}

type MatchErr int

const (
	OnlyMatched MatchErr = iota
	OnlySrc
	Non
)

type MatchError struct {
	errType MatchErr
}

func (e MatchError) Error() string {
	switch e.errType {
	case OnlyMatched:
		{
			return "Matched but did not find img source"
		}
	case OnlySrc:
		{
			return "Got Source but did not match"
		}
	case Non:
		{
			return "Did not match or find source"
		}
	default:
		{
			return "Undefined Match error"
		}
	}
}

func NewMatchError(e MatchErr) MatchError {
	return MatchError{errType: e}
}
