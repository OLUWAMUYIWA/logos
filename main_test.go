package main

import "testing"

func TestParseAddrsFromFile(t *testing.T) {
	addrs, err := parseAddrsFromFile("./testfiles/addrs2.txt")
	if err != nil {
		t.Errorf("could not parse: %s", err)
	}

	expected := []string{
		"facebook.com",
		"twitter.com",
		"ask.com",
		"att.com",
		"go.com",
		"indeed.com",
		"verizonwireless.com",
		"godaddy.com",
		"directv.com",
	}

	for i := range addrs {
		if addrs[i] != expected[i] {
			t.Errorf("Wrong. Should be %s, but is %s", expected[i], addrs[i])
		}
	}
}

func TestParseAddrsFromStdIn(t *testing.T) {
	addrs, err := parseAddrsFromStdIn("facebook.com, twitter.com,ask.com,att.com,go.com,indeed.com,verizonwireless.com,godaddy.com,directv.com")
	if err != nil {
		t.Errorf("could not parse: %s", err)
	}
	expected := []string{
		"facebook.com",
		"twitter.com",
		"ask.com",
		"att.com",
		"go.com",
		"indeed.com",
		"verizonwireless.com",
		"godaddy.com",
		"directv.com",
	}
	for i := range addrs {
		if addrs[i] != expected[i] {
			t.Errorf("Wrong. Should be %s, but is %s", expected[i], addrs[i])
		}
	}
}
