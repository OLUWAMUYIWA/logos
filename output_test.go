package main

import (
	"encoding/csv"
	"os"
	"testing"
)

func TestCsv(t *testing.T) {
	csvWtr, err := NewCsvAriter("./testfiles/csvtest")
	if err != nil {
		t.Error(err)
	}
	results := produceResults()
	err = csvWtr.WriteResults(results)
	if err != nil {
		t.Error(err)
	}

	f, err := os.Open("./testfiles/csvtest")
	if err != nil {
		t.Error(err)
	}
	rdr := csv.NewReader(f)

	all, err := rdr.ReadAll()
	if err != nil {
		t.Error(err)
	}
	expected := [][]string{
		{"facebook.com", "facebook.jpg", "facebook.fav"},
		{"twitter.com", "twitter.jpg", "twitter.fav"},
		{"ask.com", "ask.jpg", "ask.fav"},
		{"indeed.com", "indeed.jpg", "indeed.fav"},
		{"godaddy.com", "godaddy.jpg", "godaddy.fav"},
	}

	if len(all) != len(expected) {
		t.Error("Wrong results, expected sme length")
	}
	for i := 0; i < len(all); i++ {
		for j := 0; j < 3; j++ {
			if all[i][j] != expected[i][j] {
				t.Errorf("Expected: %s. Got: %s", expected[i][j], all[i][j])
			}
		}
	}
}

func produceResults() <-chan Result {
	rChan := make(chan Result)
	results := []Result{
		{domain: "facebook.com", logoUrl: "facebook.jpg", favUrl: "facebook.fav"},
		{domain: "twitter.com", logoUrl: "twitter.jpg", favUrl: "twitter.fav"},
		{domain: "ask.com", logoUrl: "ask.jpg", favUrl: "ask.fav"},
		{domain: "indeed.com", logoUrl: "indeed.jpg", favUrl: "indeed.fav"},
		{domain: "godaddy.com", logoUrl: "godaddy.jpg", favUrl: "godaddy.fav"},
	}

	go func() {
		for _, r := range results {
			rChan <- r
		}
		close(rChan)
	}()
	return rChan
}
