package main

import (
	"bufio"
	"bytes"
	"fmt"
	"log"
	"path"
	"sync"

	"flag"
	"os"
	"strings"
)

func main() {

	// set up flags
	fromFile := flag.Bool("file", false, "Indicates that the program should expect a file name and not a  list of comma-separated urls")
	flag.Parse()

	if len(os.Args) < 2 {
		log.Fatalln("Must input either comma-separated list of urls or a path to  url file")
	}
	pathOrUrl := flag.Arg(0)
	if pathOrUrl == "" {
		log.Fatalln("Program unable to continue. Provide  a path or comma-separated url list as argument")
	}

	log.Println("Scraping....")

	var addrs []string
	var err error
	if *fromFile {
		addrs, err = parseAddrsFromFile(pathOrUrl)
		if err != nil {
			fmt.Printf("Could not parse from file: %s", err)
			os.Exit(1)
		}
	} else {
		addrs, err = parseAddrsFromStdIn(pathOrUrl)
		if err != nil {
			fmt.Printf("Could not parse from stdin: %s", err)
			os.Exit(1)
		}
	}

	scraper := NewScraper(addrs)

	kvs, errs := scraper.StreamGet(20)

	// handle all network errors by logging them in a .csv file
	// we might choose to retry based on err type
	// define error path
	errPath := ""
	if *fromFile {
		dir, fname := path.Split(pathOrUrl)
		fname = strings.TrimSuffix(fname, ".csv")
		fname = fname + "_errors.csv"
		errPath = path.Join(dir, fname)
	} else {
		errPath = "errorlog.csv"
	}

	// wait for two goroutines
	var wg sync.WaitGroup
	wg.Add(2)

	go func() {
		LogErrors(errs, errPath)
		wg.Done()
	}()

	// scrape
	results := scraper.Scrape(kvs)

	// define output path
	outputPath := ""
	if *fromFile {
		dir, fname := path.Split(pathOrUrl)
		fname = strings.TrimSuffix(fname, ".csv")
		fname = fname + "_results.csv"
		outputPath = path.Join(dir, fname)
	} else {
		outputPath = "logos.csv"
	}
	wtr, err := NewCsvAriter(outputPath)

	if err != nil {
		log.Fatalf("Could not create csv writer: %s\n", err)
	}

	// write results
	go func() {
		wtr.WriteResults(results)
		wg.Done()
	}()

	wg.Wait()

	log.Printf("Done. Results written to: %s", outputPath)
}

func parseAddrsFromFile(path string) ([]string, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)
	scanner.Split(bufio.ScanLines)
	var addrs []string

	for scanner.Scan() {
		txt := scanner.Text()
		txt = Trim(txt, " ")
		addrs = append(addrs, txt)
	}
	return addrs, nil
}

func parseAddrsFromStdIn(s string) ([]string, error) {
	rdr := strings.NewReader(s)

	scanner := bufio.NewScanner(rdr)
	scanner.Split(func(data []byte, atEOF bool) (advance int, token []byte, err error) {
		if atEOF {
			return 0, data, bufio.ErrFinalToken
		}
		idx := bytes.IndexRune(data, ',')
		if idx == -1 { // not yet at EOF so we request more data by sending nil as token value
			return 0, nil, nil
		}
		tok := data[:idx]
		return len(tok) + 1, tok, nil
	})
	var addrs []string

	for scanner.Scan() {
		txt := scanner.Text()
		txt = Trim(txt, " ")
		addrs = append(addrs, txt)
	}
	return addrs, scanner.Err()
}

func Trim(s string, cutset string) string {
	if s == "" || cutset == "" {
		return s
	}
	return strings.TrimFunc(s, func(r rune) bool { return strings.IndexRune(cutset, r) >= 0 })
}
