package main

import (
	"bytes"
	"io"
	"log"
	"os"
	"strings"
	"testing"

	"golang.org/x/net/html"
)

var docStr string = `
<!doctype html>
<html dir="ltr" lang="en">
  <head>
    <meta charset="utf-8">
    <title>New Tab</title>
  </head>
  <body>
    <script type="module" src="new_tab_page.js"></script>
    <link rel="stylesheet" href="chrome://resources/css/text_defaults_md.css">
    <link rel="stylesheet" href="shared_vars.css">
    <img src="../images/badger.png" alt="dgraph" class="logo">
  </body>
</html>
`

func getDocRdr(s string) io.Reader {
	return strings.NewReader(s)
}

func TestMatches(t *testing.T) {
	doc, _ := NewDocument(getDocRdr(docStr))
	logo, _ := doc.Match()
	exp := "../images/badger.png"
	if string(logo) != exp {
		t.Errorf("Should be %s, but is %s", exp, string(logo))
	}
}

var testImg = `
<!doctype html>
<html lang="en">
<head></head>
<body>
<img src="../images/badger.png" alt="dgraph" class="logo">
</body>
</html>
`

func TestImgClass(t *testing.T) {
	rdr := bytes.NewReader([]byte(testImg))
	log.SetOutput(os.Stderr)
	doc, _ := html.Parse(rdr)

	s := ""
	var f func(*html.Node)
	f = func(n *html.Node) {
		log.Printf("Data: %s", n.Data)
		if n.Type == html.ElementNode && n.Data == "img" {
			s = imgClass(n)
		}
		for c := n.FirstChild; c != nil; c = c.NextSibling {
			f(c)
		}
	}
	f(doc)
	exp := "../images/badger.png"
	if s != exp {
		t.Errorf("Should be: %s, but is %s", exp, s)
	}
}

var testAlt = `
<!doctype html>
<html lang="en">
<head></head>
<body>
<img src="../images/badger.png" alt="logo" class="another">
</body>
</html>
`

func TestImageAlt(t *testing.T) {
	rdr := bytes.NewReader([]byte(testAlt))
	log.SetOutput(os.Stderr)
	doc, _ := html.Parse(rdr)

	s := ""
	var f func(*html.Node)
	f = func(n *html.Node) {
		log.Printf("Data: %s", n.Data)
		if n.Type == html.ElementNode && n.Data == "img" {
			s = Alt(n, imgAlt, imgSource, imgClass)
		}
		for c := n.FirstChild; c != nil; c = c.NextSibling {
			f(c)
		}
	}
	f(doc)
	exp := "../images/badger.png"
	if s != exp {
		t.Errorf("Should be: %s, but is %s", exp, s)
	}
}

var testFav = `
<!doctype html>
<html lang="en">
<head></head>
<body>
<link rel="icon" type="image/x-icon" href="/images/favicon.ico">
</body>
</html>
`

func TestFavicon(t *testing.T) {
	rdr := bytes.NewReader([]byte(testFav))
	log.SetOutput(os.Stderr)
	doc, _ := html.Parse(rdr)

	s := ""
	var f func(*html.Node)
	f = func(n *html.Node) {
		log.Printf("Data: %s", n.Data)
		if n.Type == html.ElementNode && n.Data == "link" {
			s = Alt(n, faviconLinkClass, faviconLinkImage)
		}
		for c := n.FirstChild; c != nil; c = c.NextSibling {
			f(c)
		}
	}
	f(doc)
	exp := "/images/favicon.ico"
	if s != exp {
		t.Errorf("Should be: %s, but is %s", exp, s)
	}
}

var testBoth = `
<!doctype html>
<html lang="en">
<head></head>
<body>
<link rel="icon" type="image/x-icon" href="/images/favicon.ico">
<img src="../images/badger.png" alt="logo" class="another">
</body>
</html>
`

func TestLogoFav(t *testing.T) {
	rdr := bytes.NewReader([]byte(testBoth))
	log.SetOutput(os.Stderr)

	doc, _ := NewDocument(rdr)

	logo, fav := doc.Match()

	expfav := "/images/favicon.ico"

	expLogo := "../images/badger.png"

	if logo != expLogo {
		t.Errorf("Should be: %s, but is: %s", expLogo, logo)
	}

	if fav != expfav {
		t.Errorf("Should be: %s, but is: %s", expfav, fav)
	}
}
