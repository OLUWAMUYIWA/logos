package main

import (
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"sync"
	"testing"

	"github.com/jarcoal/httpmock"
)

func TestStreamGet(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	httpmock.RegisterResponder("GET", "https://facebook.com", HtmlRespnderOk())
	httpmock.RegisterResponder("GET", "https://twitter.com", HtmlRespnderOk())
	httpmock.RegisterResponder("GET", "https://ask.com", HtmlRespnderOk())
	httpmock.RegisterResponder("GET", "https://att.com", HtmlRespnderOk())
	httpmock.RegisterResponder("GET", "https://go.com", HtmlRespnderOk())
	httpmock.RegisterResponder("GET", "https://indeed.com", HtmlRespnderOk())
	httpmock.RegisterResponder("GET", "https://verizonwireless.com", HtmlRespnderOk())
	httpmock.RegisterResponder("GET", "https://godaddy.com", HtmlRespnderOk())
	httpmock.RegisterResponder("GET", "https://directv.com", HtmlRespnderOk())

	addrs := []string{
		"facebook.com",
		"twitter.com",
		"ask.com",
		"att.com",
		"go.com",
		"indeed.com",
		"verizonwireless.com",
		"godaddy.com",
		"directv.com",
	}

	log.SetOutput(os.Stderr)
	scraper := NewDefaultScraper(addrs)
	var wg sync.WaitGroup
	wg.Add(2)
	kvs, errs := scraper.StreamGet(4)
	go func() {
		for kv := range kvs {
			log.Printf("recvd: %s: %s\n", kv.k, kv.v[:10])
		}
		wg.Done()
	}()
	go func() {
		for err := range errs {
			log.Printf("errored: %s: %s", err.addr, err.err)
		}
		wg.Done()
	}()
	wg.Wait()

	count := httpmock.GetTotalCallCount()
	if count != 9 {
		t.Errorf("not all calls made: %d", count)
	}

	_ = httpmock.GetCallCountInfo()

}

// just for testing purposes because httpmock replaces DefaultClient
func NewDefaultScraper(addrs []string) *Scraper {
	// preprocess addrs
	pref := "https://"
	for i := 0; i < len(addrs); i++ {
		addr := addrs[i]
		if !strings.HasPrefix(addr, pref) {
			addrs[i] = strings.Join([]string{pref, addr}, "")
		}
	}

	return &Scraper{
		addrs, http.DefaultClient,
	}
}

func HtmlRespnderOk() httpmock.Responder {
	return httpmock.ResponderFromResponse(&http.Response{
		Status:     strconv.Itoa(200),
		StatusCode: 200,
		Body:       httpmock.NewRespBodyFromBytes([]byte(okHtml)),
		Header: http.Header{
			"Content-Type": []string{"text/html"},
		},
		ContentLength: -1,
	})
}

func HtmlRespnderErr() httpmock.Responder {
	return httpmock.ResponderFromResponse(&http.Response{
		Status:     strconv.Itoa(400),
		StatusCode: 400,
		Body:       httpmock.NewRespBodyFromBytes(nil),
		Header: http.Header{
			"Content-Type": []string{"text/html"},
		},
		ContentLength: -1,
	})
}

func HtmlRespnderBadType() httpmock.Responder {
	return httpmock.ResponderFromResponse(&http.Response{
		Status:     strconv.Itoa(200),
		StatusCode: 200,
		Body:       httpmock.NewRespBodyFromBytes([]byte(okHtml)),
		Header: http.Header{
			"Content-Type": []string{"application/json"},
		},
		ContentLength: -1,
	})
}

var okHtml string = `
<!doctype html>
<html dir="ltr" lang="en">
  <head>
  </head>
  <body>
  </body>
</html>
`
